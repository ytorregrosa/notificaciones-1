package com.yth.mastermoviles.notificaciones;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    EditText input;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        input = (EditText)findViewById(R.id.textInput);
        Button boton = (Button)findViewById(R.id.boton);

        boton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                if (!input.getText().toString().equals("")) {
                    LayoutInflater inflater = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    View layout = inflater.inflate(R.layout.toast, null);
                    TextView toastText = (TextView) layout.findViewById(R.id.toastText);
                    toastText.setText(input.getText().toString());
                    Toast toast3 = new Toast(getApplicationContext());
                    toast3.setDuration(Toast.LENGTH_SHORT);
                    toast3.setView(layout);
                    toast3.setGravity(Gravity.CENTER, 0, 0);
                    toast3.show();
                    input.setText("");
                }
            }
        });
    }
}
